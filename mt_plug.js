// ==UserScript==
// @name     MT script
// @version  1
// @grant    none
// @require    https://cdn.bootcss.com/jquery/3.4.1/jquery.min.js
// @run-at document-idle
// @author karlwang@rayootech.com

// ==/UserScript==
$(function(){
  
	
  $("caption").append('<input id="auto" type="button" value="auto"> <input id="btn" type="submit" value="submit">');
  	addBtnEvent("auto");
  	addBtnEvent("btn");
  
		$("input[name='add_title']").val('TKE_VIEW');

  	
		$("[name=task_type]").find("option").eq(0).prop("selected",true);
    $("[name=tke_lead_id]").find("option").eq(0).prop("selected",true);


    $("input[name='plan_cost_time']").val('8');

})

function addBtnEvent(id){
					if(id=='auto'){
            
            		//localStorage.setItem("key","value")
            
          			$("#"+id).bind("click",function(){
                  /*
                  var stardata = localStorage.getItem("startdata");
                  console.log(stardata);
              if(stardata){
                  var d=new Date(startdata); 
                      d.setDate(d.getDate()+1);
                      $("#plan_start_date").val(d.Format("yyyy-MM-dd"));
              }
                  */
                $("[name=task_type]").find("option").eq(0).prop("selected",true);
                 var startdata = $("#plan_start_date").val();
                $("#target_date").val(startdata);
                $("#completed").attr("checked",'true');
                 $("[name=tke_lead_id]").find("option").eq(0).prop("selected",true);
                $('#task_complete_time').show();

                $("#practical_finish_date").val($("#plan_start_date").val());
                var str = $("[name='description']").val();
                //match ticket no
                var ticket = str.match(/\d{7}/);
                  if(!ticket){
                  	ticket = str.match(/\d{6}/);
                  }
                //fill ticket
                $("[name=itcm_no]").val(ticket[0]);
                 
                localStorage.setItem("startdata",startdata);
                  
              
            });
          
          }else if(id=='btn'){
            $("#"+id).bind("click",function(){
                console.log('aaa');
          	//http://114.247.139.178:8088/mt/module/worktime_management/list.php?action=add_task&where=my_task
            window.open("http://114.247.139.178:8088/mt/module/worktime_management/list.php?action=add_task&where=my_task","_blank");
              
            });
            
          }
            
}

//format date
  Date.prototype.Format = function (fmt) { //author: meizz
		  var o = {
		    "M+": this.getMonth() + 1, //月份
		    "d+": this.getDate(), //日
		    "h+": this.getHours(), //小时
		    "m+": this.getMinutes(), //分
		    "s+": this.getSeconds(), //秒
		    "q+": Math.floor((this.getMonth() + 3) / 3), //季度
		    "S": this.getMilliseconds() //毫秒
		  };
		  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
		  for (var k in o)
		  if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
		  return fmt;
		}

